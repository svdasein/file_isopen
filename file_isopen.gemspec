require_relative 'lib/file_isopen/version'

Gem::Specification.new do |spec|
  spec.name          = "file_isopen"
  spec.version       = FileIsopen::VERSION
  spec.authors       = ["Dave Parker"]
  spec.email         = ["daveparker01@gmail.com"]

  spec.summary       = %q{Determine if a file has any open descriptors associated with it on a linux system}
  spec.homepage      = "https://gitlab.com/svdasein/file_isopen"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.3.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_development_dependency("pry")
end
