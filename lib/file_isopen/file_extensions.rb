module File::IsOpen
    def self.included(base)
        base.extend(ClassMethods)
    end

    module ClassMethods
        def is_open?(aPath=nil)
            Dir.glob('/proc/*/fd/*').collect { |fn|File.symlink?(fn) ? File.readlink(fn):nil}.reject{|each|each.nil? or each =~ /^socket:|^\/dev\/|^pipe:|^anon_inode:/}.to_set.include?(aPath)
        end
    end
end
