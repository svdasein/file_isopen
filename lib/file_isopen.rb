require "file_isopen/version"
require "file_isopen/file_extensions"

File::class_eval do
    include File::IsOpen
end
