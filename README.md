# FileIsopen

This gem adds a class method to the File class - File.is_open?(aPath).  This lets you check to see if a file aPath has any file descriptors open on it on the whole system.  This is linux specific and uses the /proc filesystem.  It's much lighter than shelling out to lsof.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'file_isopen'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install file_isopen

## Usage

```
require 'file_isopen'

while File.is_open?('/my/file.dat') do
  sleep(1)
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/file_isopen.

